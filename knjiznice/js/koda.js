
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var appId = "TomGorn";
var loadedPatients = [];
var bmiGroups = [];
var novPacient = {};
var crkeId = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
var crka = crkeId.charAt(Math.floor(Math.random() * crkeId.length));  
var ind = Math.floor(Math.random() * 10000);



/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS tev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, odgovor) {
    appId = "TomGorn"+ crka + ind;
    
	var namesM = ["Janez", "Martin", "Peter", "Bojan", "Borut", "Aleš", "Matej", "Robert", "Luka"];
	var namesF =["Mojca", "Nina", "Mateja", "Andreja", "Katja", "Ana", "Marija"];
	var surnames = ["Novak", "Horvat", "Kovač", "Mlakar", "Kos", "Vidmar", "Golob", "Hribar", "Zupan", "Kastelic", "Oblak", "Petek", "Koren"];
	var births = ["1982-7-18T19:30", "1987-5-18T19:30", "1984-1-23T19:30", "1993-5-25T19:30", "2000-9-10T19:30", "1999-2-11T12:30"];
	var mesta = [
	    {
	        name: "Ljubljana",
	        lat: 46.0569465,
	        lon: 14.505751499999974
	    }, {
	        name: "Maribor",
	        lat: 46.5546503,
	        lon: 15.645881199999963
	    }, {
	        name: "Celje",
	        lat: 46.23974949999999,
	        lon: 15.267706299999986,
	    }, {
	        name: "Kranj",
	        lat: 46.2428344,
	        lon: 14.355541700000003
	    }, {
	        name: "Koper",
	        lat: 45.54805899999999,
	        lon: 13.730187699999988
	    }, {
	        name: "Novo mesto",
	        lat: 45.8010824,
	        lon: 15.17100890000006
	    }, {
	        name: "Murska Sobota",
	        lat: 46.6581381,
	        lon: 16.161029299999996
	    }, {
	        name: "Nova Gorica",
	        lat: 45.9549755,
	        lon: 13.649304400000005
	    }, {
	        name: "Krško",
	        lat: 45.95899780000001,
	        lon: 15.492131200000017
	    }, {
	        name: "Jesenice",
	        lat: 46.4367047,
	        lon: 14.052605699999958
	    }
	    
	    ];
	var sex = ["MALE", "FEMALE"];
	var sessionId = getSessionId();
		
	var gender = sex[Math.floor(Math.random()*sex.length)];
	var maxVisina;
	var minVisina;
	var maxTeza = 110;
	var minTeza = 50;
	
	if (gender == "MALE") {
		var ime = namesM[Math.floor(Math.random()*namesM.length)];
		var maxVisina = 200;
		var minVisina = 168;
	}
	else {
		var ime = namesF[Math.floor(Math.random()*namesF.length)];
		var maxVisina = 180;
		var minVisina = 155;
	}
	var priimek = surnames[Math.floor(Math.random()*surnames.length)];
	var datumRojstva = births[Math.floor(Math.random()*births.length)];
	var mesto = mesta[Math.floor(Math.random()*mesta.length)];
	var telesnaVisina = Math.floor(Math.random()*(maxVisina-minVisina+1)+minVisina);
	var telesnaTeza = Math.floor(Math.random()*(maxTeza-minTeza+1)+minTeza);
	var datumInUra = "2014-3-19T13:10Z";
	
	
	$.ajaxSetup({
	 headers: {"Ehr-Session": sessionId}
	});
		
    
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function (data) {
            var ehrId = data.ehrId;
            ehrGlobal=data.ehrId;
            //$("#header").html("EHR: " + ehrId);
    
            // build party data
            var partyData = {
                firstNames: ime,
                lastNames: priimek,
                dateOfBirth: datumRojstva,
                gender: gender,
        		address: {      
        			address: JSON.stringify({
        			    mesto: mesto.name,
        			    lattitude: mesto.lat,
        			    longitude: mesto.lon
        			}),
        		},
                partyAdditionalInfo: [
                    {
                        key: "ehrId",
                        value: ehrId
                    },
                    {
                        key: "friAppId",
                        value: appId
                    }
                ]
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    ustvariVitalSignsZaPacienta(ehrId, telesnaVisina, telesnaTeza, function(){
                        odgovor(); 
                    });
                },
                error: function(){
                    console.log("Error saving party.");
                }
            });
        }
    });
}

function ustvariVitalSignsZaPacienta(ehrId, telesnaVisina, telesnaTeza, successCallback)
{
    var compositionData = {
        "ctx/time": "2014-3-19T13:10Z",
        "ctx/language": "en",
        "ctx/territory": "CA",
        "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
        "vital_signs/body_weight/any_event/body_weight": telesnaTeza
    };
    var queryParams = {
        "ehrId": ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: 'Belinda Nurse'
    };
    $.ajax({
        url: baseUrl + "/composition?" + $.param(queryParams),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(compositionData),
        success: function (res) {
            successCallback();
        }
    });
}

function kreirajEHR(index)
{
    if(index == 0) document.getElementById("generiraj").innerHTML="Nalaganje...";
    document.getElementById("prikaziSeznam").disabled = false;
    
   if(index < 20) {
       generirajPodatke(index, function(){
           kreirajEHR(index+1);
       });
    if(index == 19) {
        document.getElementById("generiraj").innerHTML="Generiranje podatkov";
        ind++;
    }
   }
}

function preberiEHRodBolnika() {
	var sessionId = getSessionId();
	document.getElementById("preberiEHR").innerHTML="Nalaganje...";

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		alert("EHR ne obstaja");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
		    novPacient = data.party;
		    $("#message").html(
                    "<span class='alert alert-success' role='alert'>" +
                    "EHR uspešno pridobljen");
            pridobiPaciente();
				
			},
			error: function(err) {
			$("#message").html(
                    "<span class='alert alert-danger' role='alert'>" +
                    "EHR ne obstaja</span>");
			}
		});
	}
}

function pridobiPaciente() {
	var sessionId = getSessionId();
    document.getElementById("prikaziSeznam").innerHTML="Nalaganje...";
    document.getElementById("prikaziGraf").disabled = false;
    document.getElementById("prikaziZemljevid").disabled = false;
    document.getElementById("preberiEHR").disabled = false;
	$.ajaxSetup({
	 headers: {"Ehr-Session": sessionId}
	});
	var queryParams = [
        {key: "friAppId", value: appId }
    ];

	$.ajax({
            url: baseUrl + "/demographics/party/query",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(queryParams),
            success: function (data) {
                if(!jQuery.isEmptyObject(novPacient)) data.parties.push(novPacient);
                console.log(data.parties);
                pripniVitalSigns(data.parties, 0, function onDone(pacienti){
                  pacienti.forEach(function parseAddressAddBMI(pacient){
                      //console.log(pacienti);
                     if(pacient.address != undefined) pacient.address = JSON.parse(pacient.address.address);
                     else $("#message").html(
                    "<span class='alert alert-warning' role='alert'>" +
                    "Vaš naslov ni podan pravilno, zato se vaši podatki ne bodo izrisali na zemljevidu</span>");
                     if(pacient.height == undefined || pacient.weight == undefined) $("#message").html(
                    "<span class='alert alert-danger' role='alert'>" +
                    "Višina ali teža nista podani</span>");
                     else pacient.BMI = izracunBMI(pacient);
                  });
                  loadedPatients = pacienti;
                  bmiGroups = generirajBMISkupine();
                  pacienti.forEach(function razvrstiVSkupino(pacient){
                    for (var i = 0; i < bmiGroups.length; i++){
                        if (bmiGroups[i].isInRange(pacient.BMI))
                        {
                            bmiGroups[i].increaseCount();
                            break;
                        }
                    }
                  });
                  izrisiPaciente(pacienti); 
                    document.getElementById("prikaziSeznam").innerHTML="Prikaži seznam bolnikov";
                    document.getElementById("preberiEHR").innerHTML= "Dodaj bolnika na seznam";
                });
            }
        }).fail(function errorHandler(error){
            console.log(error);
        });
}

 
// poklici view za vital signs, ko dobiš success, pokliči še drug
// API in pridobi drug podatek
// ko si tudi tistega uspešno zaključil, pripni podatek na pacient
// objekt .. pacient.weight = x; pacient.height = x;
// in pokliči callback(); da signaliziraš, da je konec
function pridobiVitalSigns(pacient, callback)
{
    var ehrId = najdiEhrId(pacient.partyAdditionalInfo);
    var sessionId = getSessionId();
    $.ajax({
        url: baseUrl +"/view/" + ehrId + "/weight",
        type: "GET",
	 headers: {"Ehr-Session": sessionId},
	 success: function(res) {
	     if (res && res.length > 0)
	     {
	         pacient.weight = res[0].weight;
	     }
	     $.ajax({
	         url: baseUrl +"/view/" + ehrId +"/height",
	         type: "GET",
	         headers: {"Ehr-Session": sessionId},
        	 success: function(result) {
        	      for(var i  in result) {
        	          pacient.height = result[i].height;
        	      }
        	      callback(pacient);
        	  }       
	     });
	 }
    });
}

function najdiEhrId(partyInfo)
{
    for (var idx = 0; idx < partyInfo.length; idx++)
    {
        if (partyInfo[idx].key === "ehrId") return partyInfo[idx].value;
    }
    
    return undefined;
}

function pripniVitalSigns(pacienti, index, doneCallback)
{
   if (pacienti.length == index) 
   { 
       doneCallback(pacienti);
   }
   else
   {
       pridobiVitalSigns(pacienti[index], function onDone(){
           index++;
           pripniVitalSigns(pacienti, index, doneCallback);
       });
   }
}

function izrisiPaciente(pacienti)
{
    var mesto;
    var visina;
    var teza;
    var bmi;
    var $pacienti = $("#pacienti");
    $pacienti.html("");
    pacienti.forEach(function ustvariVrstico(pacient){
        var vrsticaPacienta = '<div class="panel panel-default">';
        
        vrsticaPacienta += '<div class="panel-heading" role="tab" id="h' + pacient.id + '">';
        vrsticaPacienta += '<h4 class="panel-title">'
        vrsticaPacienta += '<a class="collapsed" role="button"';
        vrsticaPacienta += ' data-toggle="collapse" data-parent="#pacienti"';
        vrsticaPacienta += ' href="#c' + pacient.id + '" aria-expanded="false" ';
        vrsticaPacienta += ' aria-controls="c' + pacient.id + '">';
        vrsticaPacienta += pacient.firstNames + ' ' + pacient.lastNames;
        vrsticaPacienta += '</a>'
        vrsticaPacienta += '</h4>'
        vrsticaPacienta += '</div>'; 
        // panel vsebina
        vrsticaPacienta += '<div id="c' + pacient.id + '" class="panel-collapse collapse"';
        vrsticaPacienta += ' role="tabpanel" aria-labelledby="h' + pacient.id + '">';
        if(pacient.address != undefined) mesto = pacient.address.mesto;
        else mesto = "Mesto stalnega prebivališča ni podano";
        if(pacient.height != undefined) visina = pacient.height;
        else visina = "Višina ni podana";
        if(pacient.weight != undefined) teza = pacient.weight;
        else teza = "Višina ni podana";
        if(pacient.BMI != undefined) bmi = Math.round(pacient.BMI*100)/100;
        else bmi = "Pacientov BMI ne obstaja";
        vrsticaPacienta += '<div class="panel-body"> <p><strong>Mesto stalnega prebivališča:</strong> '+ mesto+'</p> <p> <strong>Telesna teža:</strong> ' + teza + ' kg </p> <p> <strong>Telesna višina:</strong> ' + visina+ ' cm </p> <p> <strong> BMI:</strong> '+bmi+' </p></div>';
       // vrsticaPacienta += '<div class="panel-body">''Telesna teža:' + pacient.weight + '|| Telesna višina' + pacient.height'</div>';
        vrsticaPacienta += '</div>';
        // konec vsebinske panele
        
        vrsticaPacienta += '</div>';
        $pacienti.append(vrsticaPacienta);
    });
}

function izracunBMI(pacient) {
    var visina = (pacient.height)/100;
    var bmi = pacient.weight/(visina*visina)
    return bmi;
}

function narisiGraf() {
    var podatki = bmiGroups.map(function vPodatke(skupina){
      return { name: skupina.naziv, y: skupina.count }  
    });
    
    Highcharts.chart('graf', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'BMI Pacientov'
        },
        subtitle: {
            text: 'BMI pacientov na sistematskem pregledu po stopnjah debelosti'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Število pacientov'
            }
    
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },
    
        //tooltip: {
        //    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        //    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        //},
    
        series: [{
            name: 'BMI',
            colorByPoint: true,
            data: podatki
        }]
    }); 
}

function generirajBMISkupine() {
    // Below 18.5	Underweight
    // 18.5 – 24.9	Normal or Healthy Weight
    // 25.0 – 29.9	Overweight
    // 30.0 and Above
    return [
        new BMISkupina("Podhranjenost", 0, 18.5, "1"),
        new BMISkupina("Normalna telesna teža", 18.5, 24.9, "2"),
        new BMISkupina("Povečana telesna teža", 24.9, 29.9, "3"),
        new BMISkupina("Debelost", 29.9, 100, "4")
        ];
}

function BMISkupina(naziv, min, max, color){
    this.naziv = naziv;
    this.count = 0;
    this.min = min;
    this.max = max;
    this.color = color;
    this.isInRange = function(vrednost){
        return vrednost > this.min && vrednost <= this.max;
    }
    this.increaseCount = function(){
        this.count++;
    }
    this.getColor = function(){
        return this.color;
    }
}

function povprecniBMI(pacienti, lokacija) {
    var sum = 0;
    var stevec = 0;
    for(var i in pacienti) {
        if(pacienti[i].address != undefined && pacienti[i].address.mesto == lokacija) {
        sum += pacienti[i].BMI;
        stevec++;
        }
    }
    return (sum/stevec)*1000;
    
}
//console.log(povprecniBMI(loadedPatients, "Ljubljana"));

function narisiZemljevid() {
    	var mesta = [
	    {
	        name: "Ljubljana",
	        lat: 46.0569465,
	        lon: 14.505751499999974
	    }, {
	        name: "Maribor",
	        lat: 46.5546503,
	        lon: 15.645881199999963
	    }, {
	        name: "Celje",
	        lat: 46.23974949999999,
	        lon: 15.267706299999986
	    }, {
	        name: "Kranj",
	        lat: 46.2428344,
	        lon: 14.355541700000003
	    }, {
	        name: "Koper",
	        lat: 45.54805899999999,
	        lon: 13.730187699999988
	    }, {
	        name: "Novo mesto",
	        lat: 45.8010824,
	        lon: 15.17100890000006,
	        
	    }, {
	        name: "Murska Sobota",
	        lat: 46.6581381,
	        lon: 16.161029299999996
	    }, {
	        name: "Nova Gorica",
	        lat: 45.9549755,
	        lon: 13.649304400000005
	    }, {
	        name: "Krško",
	        lat: 45.95899780000001,
	        lon: 15.492131200000017
	    }, {
	        name: "Jesenice",
	        lat: 46.4367047,
	        lon: 14.052605699999958
	    }
	    
	    ];
var heatMapData = [
  {location: new google.maps.LatLng(mesta["0"].lat, mesta["0"].lon), weight:povprecniBMI(loadedPatients, mesta["1"].name)},
  {location: new google.maps.LatLng(mesta["1"].lat, mesta["1"].lon), weight: povprecniBMI(loadedPatients, mesta["1"].name)},
  {location: new google.maps.LatLng(mesta["2"].lat, mesta["2"].lon), weight: povprecniBMI(loadedPatients, mesta["2"].name)},
  {location: new google.maps.LatLng(mesta["3"].lat, mesta["3"].lon), weight: povprecniBMI(loadedPatients, mesta["3"].name)},
  {location: new google.maps.LatLng(mesta["4"].lat, mesta["4"].lon), weight: povprecniBMI(loadedPatients, mesta["4"].name)},
  {location: new google.maps.LatLng(mesta["5"].lat, mesta["5"].lon), weight: povprecniBMI(loadedPatients, mesta["5"].name)},
  {location: new google.maps.LatLng(mesta["6"].lat, mesta["6"].lon), weight: povprecniBMI(loadedPatients, mesta["6"].name)},
  {location: new google.maps.LatLng(mesta["7"].lat, mesta["7"].lon), weight: povprecniBMI(loadedPatients, mesta["7"].name)},
  {location: new google.maps.LatLng(mesta["8"].lat, mesta["8"].lon), weight: povprecniBMI(loadedPatients, mesta["8"].name)},
  {location: new google.maps.LatLng(mesta["9"].lat, mesta["9"].lon), weight: povprecniBMI(loadedPatients, mesta["9"].name)}
];




var slovenija = new google.maps.LatLng(46.15124099999999, 14.995462999999972);

map = new google.maps.Map(document.getElementById('map'), {
  center: slovenija,
  zoom: 8,
  mapTypeId: 'terrain'
});

var heatmap = new google.maps.visualization.HeatmapLayer({
  data: heatMapData,
  radius: 50,
});
heatmap.setMap(map);
}
// Google maps API key: AIzaSyDkil-6EX-SqwK1PQgFm-CKJZuva_4_fZE

// Stopnje BMI https://www.cdc.gov/healthyweight/assessing/bmi/adult_bmi/ 